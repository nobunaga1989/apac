/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apac.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public class ReverseWords {

    private int numberOfCases;
    private List<Sentence> sentences = new ArrayList();

    public void getRevesedSentence() {
        int i = 1;
        for (Sentence s : sentences) {
            
            try {
                boolean firstWord = true;
                while (true) {
                    if(firstWord){
                        System.out.print("Case #"+i++ +": ");
                        System.out.print(s.getWord());
                        firstWord = false;
                    }
                    else{
                        System.out.print(" "+s.getWord());
                    }
                }
            } catch (EmptyStackException em) {
                System.out.println("");
            }

        }
    }

    public void getWord() {

        try (BufferedReader br = new BufferedReader(new FileReader("data/B-large-practice.in"))) {
            numberOfCases = Integer.parseInt(br.readLine());
            for (int i = 0; i < numberOfCases; i++) {
                Sentence sentence = new Sentence();
                List<String> items = Arrays.asList(br.readLine().split(" "));
                for (String s : items) {
                    sentence.addWord(s);

                }
                sentences.add(sentence);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private class Sentence {

        Stack<String> words = new Stack();

        public void addWord(String word) {
            words.push(word);
        }

        public String getWord() {
            return words.pop();
        }
    }
}
