# README #


### What is this repository for? ###

* This is a Java solution for [code jam 2010 @ Africa](https://code.google.com/codejam/contest/351101/dashboard)

### How do I get set up? ###
* All of results are printed out in console log, just copy them to any text editor.
* A default file for input is a large one. You can change to a small one for each question.