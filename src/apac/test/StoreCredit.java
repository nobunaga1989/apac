/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apac.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public class StoreCredit {


    public void getStore() {
        
        try (BufferedReader br = new BufferedReader(new FileReader("data/A-large-practice.in"))) {
            //read case number
            int numberOfStore = Integer.parseInt(br.readLine());

            for (int storeCounter = 0; storeCounter < numberOfStore; storeCounter++) {
                Store store = new Store();
                store.setCredit(Integer.parseInt(br.readLine()));
                store.setNumberOfItem(Integer.parseInt(br.readLine()));

                List<String> items = Arrays.asList(br.readLine().split(" "));

                for (String s : items) {
                    Item item = new Item();
                    item.setPrice(Integer.parseInt(s));
                    store.addItem(item);
                }

                store.findMaximum(storeCounter+1);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private class Store {

        private int numberOfItem;
        private List<Item> items;

        private int credit;

        public int getCredit() {
            return credit;
        }

        public void setCredit(int credit) {
            this.credit = credit;
        }

        public Store() {
            items = new ArrayList<>();
        }

        public int getNumberOfItem() {
            return numberOfItem;
        }

        public void setNumberOfItem(int numberOfItem) {
            this.numberOfItem = numberOfItem;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        public void addItem(Item item) {
            items.add(item);
        }

        public void findMaximum(int caseNumber) {
            int item1 = 0, item2 = 0;
            int maximum = 0;
            for (int i = 0; i < numberOfItem - 1; i++) {
                for (int j = 0; j < numberOfItem; j++) {
                    if (j != i) {
                        int sumOfPrice = items.get(i).getPrice() + items.get(j).getPrice();

                        if ((sumOfPrice <= credit) && (sumOfPrice >= maximum)) {
                            item1 = i + 1;
                            item2 = j + 1;
                            //swap value in case item1 is greter than item2
                            if (item1 > item2) {
                                item1 = j + 1;
                                item2 = i + 1;
                            }
                            maximum = sumOfPrice;
                        }
                    }
                }
                

            }
            
            System.out.println("Case #"+caseNumber+": "+item1+" "+item2);

        }

    }

    private class Item {

        private int price;

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

    }

}
