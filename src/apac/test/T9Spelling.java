/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apac.test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author pc
 */
public final class T9Spelling {

    private Map<Character, String> T9Charactors = new HashMap<Character, String>();
    private List<String> sentences = new ArrayList();

    private int numberOfCases;

    public T9Spelling() {
        constructCharactors();
        readFile();
    }

    public void constructCharactors() {
        char c = 'a';
        for (int button = 2; button <= 9; button++) {
            for (int i = 0; i <= 2; i++) {
                // case 'S'
                if (c == 's') {
                    T9Charactors.put(Character.valueOf(c), String.valueOf(7777));
                    i = 3;
                    button--;
                } else {
                    switch (i) {
                        case 0:
                            T9Charactors.put(Character.valueOf(c), String.valueOf(button));

                            break;
                        case 1:
                            T9Charactors.put(Character.valueOf(c), String.valueOf(button) + String.valueOf(button));
                            break;
                        case 2:
                            T9Charactors.put(Character.valueOf(c), String.valueOf(button) + String.valueOf(button) + String.valueOf(button));
                            break;
                    }

                }
                c++;

            }
            T9Charactors.put('z', String.valueOf(9999));
            T9Charactors.put(' ', String.valueOf(0));
        }

    }

    public void printMap() {
        for (Character name : T9Charactors.keySet()) {

            String key = name.toString();
            String value = T9Charactors.get(name).toString();
            System.out.println(key + " " + value);

        }
    }

    public void readFile() {
        try (BufferedReader br = new BufferedReader(new FileReader("data/C-large-practice.in"))) {
            numberOfCases = Integer.parseInt(br.readLine());
            for (int i = 0; i < numberOfCases; i++) {

                sentences.add(br.readLine());

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StoreCredit.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void translateToT9() {
        char prevChar = 0;
        int i = 1;
        boolean firstTime = true;
        for (String s : sentences) {
            
            System.out.print("Case #"+i++ +": ");
            char[] chars = s.toCharArray();
            firstTime = true;
            for (char c : chars) {
                if (firstTime) {
                    prevChar = T9Charactors.get(c).charAt(0);
                    System.out.print(T9Charactors.get(c));
                    firstTime = false;
                }
                else{
                    if(T9Charactors.get(c).charAt(0) == prevChar){
                        System.out.print(" "+T9Charactors.get(c));
                        prevChar = T9Charactors.get(c).charAt(0);
                    }
                    else{
                        System.out.print(T9Charactors.get(c));
                        prevChar = T9Charactors.get(c).charAt(0);
                    }
                }
            }
            System.out.println("");
        }
    }

}
